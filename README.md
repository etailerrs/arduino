**Welcome**

You can find more details on [Etailer](https://www.etailer.rs)

---

## Arduino

My notes about Arduino

---

## MQTT + WiFiManager + NodeMCU

MQTT Project implemented using Mosquitto MQTT Message Broker running on Ubuntu 18.04 server in cloud. NodeMCU is running WiFiManager
library and PubSubClient to configure WiFi parameters and connect to MQTT Broker. You can write web based GUI using Paho JavaScript or
run Android MQTT Dash application on mobile phone to control NodeMCU device.

---

## IoT NodeMCU

IoT proof of concept using NodeMCU connected to WiFi home network and hosting web server to control relay and power on/off 220 VAC devices.
NodeMCU has a bug when using Serial.print. It must be configured as 74880bps, otherwise it will print garbage to serial.

---

## Smart Socket

Arduino Nano is connected to Ai-Thinker A6 GSM modem and Relay. After SMS with a command (ON/OFF) it will check who's sending and if auhorized it will turn the power (220V) on or off.

---

SoftwareSerialEtailer.h is updated header file with increased RX buffer limit. It will resolve problem with received SMS messages. Without increased buffer size, received SMS messages will be empty.

---

## PIR Motion Sensor

Arduino Nano connected to PIR Sensor, Relay and RGB Leds.

